import Routes from './routers'
import Header from './components/global/Header';
import { Provider } from 'react-redux'
import { PersistGate } from 'redux-persist/lib/integration/react'
import store from './stores/store'
import { ConnectedRouter } from 'connected-react-router'
import {history} from './routers/history'
import 'bootstrap/dist/css/bootstrap.min.css';

const persistStore = store()

 
function App() {
  return (
    <>
      <Provider store={persistStore.store}>
        <PersistGate persistor={persistStore.persistor}>
          <ConnectedRouter history={history}>
            <Header />
            <Routes />
          </ConnectedRouter>
        </PersistGate>
      </Provider>
      
    </>
  );
}

export default App;
