import axios from '../index'
import {GETTODO,ADDTODO,EDITTODO,DELETETODO} from './type'

export const getAllTodo = () => async (dispatch)=>{
  let {data} = await axios.get('/todo')
  dispatch({
    type:GETTODO,
    payload:data
  })
}

export const createTodo = (todo) => async (dispatch)=>{
  let {data} = await axios.post('/todo', todo)
  dispatch({
    type:ADDTODO,
    payload:data
  })
}


export const editTodo = (todo) => async (dispatch)=>{
  let {data} = await axios.patch(`/todo/${todo._id}`, {...todo})
  dispatch({
    type:EDITTODO,
    payload:data
  })
}

export const deleteTodo = ({_id}) => async (dispatch)=>{
  let res = await axios.delete(`/todo/${_id}`)
  dispatch({
    type:DELETETODO,
    payload:_id
  })
}