import { GETTODO,ADDTODO,EDITTODO,DELETETODO } from "../actions/type"
import {getAddData,getUpdateData,getDeleteData} from './../../hepers/StateController'

let INTIAL_STATE={
  todos:[],
  todo:'',
}

export default (state = INTIAL_STATE, action) =>{
  switch(action.type){
    case GETTODO:
      return {...state, todos:action.payload}

    case ADDTODO:{
      let todos = getAddData(state,action.payload)
      return {...state, todos}
    }

    case EDITTODO:{
      let todos = getUpdateData(state, action.payload)
      return {...state, todos}
    }
      
    case DELETETODO:{
      let todos = getDeleteData(state,action.payload)
      return {...state, todos}
    }
    
    default:
      return state
  }
}