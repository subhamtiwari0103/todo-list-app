import {combineReducers} from 'redux'
import defaultReducer from './defaultReducer'
import {connectRouter} from 'connected-react-router'
import todoReducer from './todoReducer'

export default (history)=> combineReducers({
  router: connectRouter(history),
  default: defaultReducer,
  todo:todoReducer,
})