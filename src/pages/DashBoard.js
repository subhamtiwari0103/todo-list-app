import React, {useEffect, useState} from 'react';
import {Container,Card,Row,Button} from 'react-bootstrap'
import AddEditTodo from '../components/todo/AddEditTodo';
import TodoDeleteModal from '../components/todo/TodoDeleteModal';
import {connect} from 'react-redux'
import {getAllTodo,createTodo,editTodo,deleteTodo } from '../stores/actions/todo'

const DashBoard = (props) => {
  const [show, setShow] = useState(false)
  const [todo, setTodo] = useState({
    'title':'',
    'text':''
  })
  const [resetTodo, ] = useState({
    'title':'',
    'text':''
  })
  const [deletedItem, setDeletedItem] = useState('')

  const addEditTodo = (event) => {
    event.preventDefault();
    if(!todo.hasOwnProperty("_id")){
      props.createTodo(todo).then(() => {
        setTodo(resetTodo)
      })
    }else{
      props.editTodo(todo).then(() => {
        setTodo(resetTodo)
      })
    }
  }

  const onEditPress = (item) =>{
    setTodo(item)
  }
  const onDeletePress = () =>{
    props.deleteTodo(deletedItem).then(() => {
      handleClose('',false)
    }).catch(err => {
      console.log('err',err)
    })
  }
  const handleClose = (item, value) => {
    setShow(value)
    if(value)
      return setDeletedItem(item)
    return setDeletedItem('')
  }
  useEffect(() => {
    props.getAllTodo()
  }, [])

  return (
    <>
      <Container>
        <h5>Note Details</h5>
        <hr/>
        <AddEditTodo 
          todo={todo}
          setTodo={setTodo}
          addEditTodo={addEditTodo}
        />
      </Container>
        <hr />
      <Container>
        <h5>TODO List</h5>
        <hr/>
        <Row>
          {props.todos.map((todo,index) =>(
            <Card key={index} style={{width:'17rem', marginLeft:10, marginBottom:10}}>
              <Card.Body>
                <Card.Title>{todo.title}</Card.Title>
                <Card.Text>
                  {todo.text}
                </Card.Text>
                <Button variant="info" onClick={() => onEditPress(todo)}>Edit</Button>{' '}
                <Button variant="danger" onClick={() => handleClose(todo, true)}>Delete</Button>{' '}
              </Card.Body>
            </Card>
          ))}
        </Row>
      </Container>
      <TodoDeleteModal 
        show={show}
        item={deletedItem}
        handleClose={handleClose}
        onDeletePress={onDeletePress}
      />
    </>
  );
}
const mapStateToProps = (state) => {
  return {...state, todos:state.todo.todos}
}

export default connect(mapStateToProps, {getAllTodo,createTodo,editTodo,deleteTodo })(DashBoard);
