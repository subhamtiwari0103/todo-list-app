import React from 'react';
import { Router, Route, Switch } from 'react-router-dom'
import DashBoard from '../pages/DashBoard';
import {history} from './history'

const Routes = (props) => {

  return (
    <Router history={history}>
      <Switch>
        <Route exact path="/" component={DashBoard} />
      </Switch>
    </Router>
  );
}

export default Routes;