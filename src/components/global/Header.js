import React, { Component } from 'react'
import { Navbar} from 'react-bootstrap'
import {useHistory, Link} from 'react-router-dom'

export default () => {
  let history = useHistory()
  return (
    <>
      <Navbar>
        <Navbar.Brand>
          <Link onClick={history.push('/')}>
            TODO APP 
          </Link>
        </Navbar.Brand>
        <Navbar.Toggle />
        <Navbar.Collapse className="justify-content-end">
          <Navbar.Text>
            Todo List
          </Navbar.Text>
        </Navbar.Collapse>
      </Navbar>
      <hr/>
    </>
    
  )
} 
