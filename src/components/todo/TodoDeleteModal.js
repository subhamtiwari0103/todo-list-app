import React from 'react';
import {Modal, Button} from 'react-bootstrap'

const TodoDeleteModal = ({show, handleClose, item, onDeletePress}) => {
  return (
    <Modal show={show} onHide={() => handleClose('', false)}>
      <Modal.Header closeButton>
        <Modal.Title>Delete Todo</Modal.Title>
      </Modal.Header>
      <Modal.Body>Are you sure you want to delete {item.title}</Modal.Body>
      <Modal.Footer>
        <Button variant="secondary" onClick={() => handleClose('', false)}>
          Close
        </Button>
        <Button variant="danger" onClick={onDeletePress}>
          Delete
        </Button>
      </Modal.Footer>
    </Modal>
  );
}

export default TodoDeleteModal;
