import React from 'react';
import {Form, Button} from 'react-bootstrap'

const AddEditTodo = ({todo,setTodo,addEditTodo}) => {
  const onChangeText = (event) => {
    let value = event.target.value
    let name = event.target.name
    setTodo(prevState => ({
      ...prevState,
      [name]:value
    }))
  }
  return (
    <Form>
      <Form.Group controlId="formBasicTitle">
        <Form.Label>Title</Form.Label>
        <Form.Control name="title" type="text" value={todo.title} onChange={onChangeText} placeholder="Enter Title" />
      </Form.Group>
      <Form.Group controlId="formBasicText">
        <Form.Label>Text</Form.Label>
        <Form.Control name="text" type="text" value={todo.text} onChange={onChangeText} placeholder="Enter Text" />
      </Form.Group>
      <Button variant="primary" type="submit" onClick={addEditTodo}>
        {todo.hasOwnProperty("_id")? 'Update': 'Submit'} 
      </Button>
    </Form>
  );
}

export default AddEditTodo;
