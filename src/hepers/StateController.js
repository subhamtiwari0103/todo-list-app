export const getAddData = (state, data) =>{
  return [...state.todos, data]
}

export const getUpdateData = (state, data) =>{
  let index = state.todos.findIndex(todo => todo._id === data._id)
  state.todos.splice(index,1, data)
  return [...state.todos]
}

export const getDeleteData = (state, _id) =>{
  state.todos = state.todos.filter(todo => todo._id !== _id)
  return [...state.todos]
}